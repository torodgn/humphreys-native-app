import * as Localization from 'expo-localization'
import i18n from 'i18n-js'

import es from './locale/es.json'
import en from './locale/en.json'

const deviceLocale: string =
  Localization.locale.indexOf('-') !== 0
    ? Localization.locale.split('-')[0]
    : Localization.locale

// Set the locale once at the beginning of your app.
i18n.locale = Localization.locale
// When a value is missing from a language it'll fallback to another language with the key present.

i18n.defaultLocale = deviceLocale === 'es' ? deviceLocale : 'en'
i18n.locale = deviceLocale === 'es' ? deviceLocale : 'en'
i18n.fallbacks = true
i18n.translations = { en, es }

export default i18n
