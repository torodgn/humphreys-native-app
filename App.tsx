import React, { useState } from 'react'
import { NavigationContainer, useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import {SafeAreaView, StatusBar, StyleSheet, Text} from 'react-native'

import Login from './screens/Login'
import Main from './screens/Main'
import Account from './screens/Account'
import { Colors } from './constants/BaseStyles'
import useCachedResources from './hooks/useCachedResources'

const RootStack = createStackNavigator()

export default function App() {
  const navigationRef = React.useRef(null)
  const isLoadingComplete = useCachedResources()
  const [signedIn, setSignedIn] = useState(false)

  if (!isLoadingComplete) {
    return true
  } else {
    return (
      <SafeAreaProvider style={styles.container}>
        <NavigationContainer
          documentTitle={{ enabled: false }}
          ref={navigationRef}
        >
          <StatusBar backgroundColor={Colors.primary} />
          {signedIn ? (
            <>
              <RootStack.Navigator>
                <RootStack.Screen
                  name="Main"
                  component={Main}
                  options={{
                    headerShown: false,
                  }}
                />
                <RootStack.Screen name="Account">
                  {(props) => <Account {...props} setSignedIn={setSignedIn} />}
                </RootStack.Screen>
              </RootStack.Navigator>
            </>
          ) : (
            <Login
              loginSubmitted={(status: boolean) => {
                setSignedIn(status)
              }}
            />
          )}
        </NavigationContainer>
      </SafeAreaProvider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
})
