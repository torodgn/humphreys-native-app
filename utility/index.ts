import { ASYNC_STORAGE_KEYS, CONFIG } from '../constants/settings'
import AsyncStorage from '@react-native-community/async-storage'
import { IssuersResponse, LoginResponse, UserResponse } from '../types'
import { queryParams } from './helpers'


export const storage = {
  async store(key: string, value: any, json: boolean = false) {
    try {
      const jsonValue = json ? JSON.stringify(value) : value
      await AsyncStorage.setItem(key, jsonValue)
    } catch (e) {
      // saving error
    }
  },
  async get(key: string, fallback: any = false, json: boolean = false) {
    try {
      const jsonValue = await AsyncStorage.getItem(key)
      if (jsonValue != null && json) {
        return JSON.parse(jsonValue)
      }

      return jsonValue != null ? jsonValue : fallback
    } catch (e) {
      // error reading value
      return fallback
    }
  },
  async forget(key: string) {
    await AsyncStorage.removeItem(key)
  },
}

export const login = async (
  username: string,
  password: string
): Promise<LoginResponse> => {
  return new Promise(async (resolve, reject) => {
    const data = {
      grant_type: CONFIG.GRANT_TYPE,
      username,
      password,
      scope: '*',
      client_id: CONFIG.CLIENT_ID,
      client_secret: CONFIG.CLIENT_SECRET,
    }

    try {
      const response = await fetch(`${CONFIG.HOST}${CONFIG.LOGIN_URI}`, {
        method: 'POST',
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers: {
          'Content-Type': 'application/json',
        },
      })
      if (response.status === 200) {
        const responseJson = await response.json()

        if (responseJson.access_token) {
          await storage.store(ASYNC_STORAGE_KEYS.APP_ACCESS, responseJson, true)
        }
        resolve(responseJson)
      } else {
        const responseText = await response.text()
        reject(responseText)
      }
    } catch (error) {
      console.log('ERROR', error)
      reject(error)
    }
  })
}

export const user = (): Promise<UserResponse> => {
  return new Promise(async (resolve, reject) => {
    try {
      const store = await storage.get(ASYNC_STORAGE_KEYS.APP_ACCESS, true, true)
      const response = await fetch(`${CONFIG.HOST}${CONFIG.USER_URI}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${store.access_token}`,
        },
      })

      if (response.status === 200) {
        const responseJson = await response.json()
        console.log(responseJson)
        resolve(responseJson)
      } else {
        const responseText = await response.text()
        reject(responseText)
      }
    } catch (error) {
      console.log('ERROR', error)
      reject(error)
    }
  })
}

export const issuers = (filters: object = {}): Promise<IssuersResponse> => {
  let params = queryParams(filters)

  return new Promise(async (resolve, reject) => {
    try {
      const store = await storage.get(ASYNC_STORAGE_KEYS.APP_ACCESS, true, true)
      const response = await fetch(
        `${CONFIG.HOST}${CONFIG.ISSUERS_URI}?${params}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${store.access_token}`,
          },
        }
      )

      //console.log(response.status)

      if (response.status === 200) {
        const responseJson = await response.json()
        //console.log(responseJson)
        resolve(responseJson)
      } else {
        const responseText = await response.text()
        reject(responseText)
      }
    } catch (error) {
      console.log('ERROR', error)
      reject(error)
    }
  })
}
