export type RootStackParamList = {
  Root: undefined
  NotFound: undefined
}

export type IconOptions = {
  [key: string]: string
}

export interface LoginResponse {
  access_token: string
  expires_in: [string, number]
}

export interface UserResponse {
  id: number
  email: string
}

export interface IssuerItemResponse {
  id: number
  nombre: string
  existencia: string
  rut: string
  created_at: string
  updated_at: string
}

export interface IssuersResponse extends Array<IssuerItemResponse> {
  data:
  ...
  any
  [];
}

export interface DefineDataParams {
  field: any;
  value: any;
}
