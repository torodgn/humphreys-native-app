import React, { useEffect, useState } from 'react'
import {
  Image,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  ScrollView,
  Keyboard,
  ActivityIndicator,
} from 'react-native'
import SubmitButton from '../components/SubmitButton'
import BaseStyles, { Colors } from '../constants/BaseStyles'
import { login, storage, user } from '../utility'
import { validateEmail } from '../utility/helpers'
import { ASYNC_STORAGE_KEYS } from '../constants/settings'
import i18n from './../config/i18n'

export default function Login({ loginSubmitted }: any) {
  const [form, setForm] = useState({
    email: '',
    password: '',
  })
  const [loading, setLoading] = useState(true)
  const [processing, setProcessing] = useState(false)
  const [keyboardOpen, setKeyboardOpen] = useState(false)
  const [problems, setProblems] = useState({
    email: false,
    password: false,
    form: false,
  })

  useEffect(() => {
    hasLoggedIn()

    Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardOpen(true)
    })
    Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardOpen(false)
    })

    return () => {
      Keyboard.removeAllListeners('keyboardDidShow')
      Keyboard.removeAllListeners('keyboardDidHide')
    }
  }, [])

  const validateInput = () => {
    const { email, password } = form

    if (email.length == 0) {
      setProblems({ email: true, password: false, form: false })
      return false
    }
    if (!validateEmail(email)) {
      setProblems({ email: true, password: false, form: false })
      return false
    }
    if (password.length === 0) {
      setProblems({ email: false, password: true, form: false })
      return false
    }

    setProblems({ email: false, password: false, form: false })
    return true
  }

  const makeLogin = async () => {
    if (validateInput()) {
      try {
        const { email, password } = form
        setProcessing(true)
        const loginResponse = await login(email, password)
        setProcessing(false)
        if (loginResponse.access_token && loginResponse.expires_in) {
          hasLoggedIn()
        } else {
          setTimeout(() => {
            setProblems({ email: false, password: false, form: true })
          }, 500)
        }
      } catch (error) {
        setProcessing(false)
        setTimeout(() => {
          setProblems({ email: false, password: false, form: true })
        }, 500)
      }
    }
  }

  const hasLoggedIn = async () => {
    const store = await storage.get(ASYNC_STORAGE_KEYS.APP_ACCESS, false, true)

    if (!store) {
      await setLoading(false)
      return
    }

    user()
      .then(async (userResult) => {
        if (userResult.id > 0) {
          await loginSubmitted(true)
          //await setLoading(false)
        }
      })
      .catch(async () => {
        await storage.forget(ASYNC_STORAGE_KEYS.APP_ACCESS)
        await loginSubmitted(false)
        await setLoading(false)
      })
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        contentContainerStyle={[
          styles.scrollView,
          keyboardOpen ? { justifyContent: 'flex-start' } : null,
        ]}
      >
        {!keyboardOpen ? (
          <View style={styles.logo}>
            <Image
              resizeMode="contain"
              style={styles.logoImage}
              source={require('../assets/images/logo.png')}
            />
          </View>
        ) : null}

        {loading ? (
          <View>
            <Text>{i18n.t('loading')}</Text>
            <ActivityIndicator />
          </View>
        ) : (
          <View style={styles.loginBox}>
            <View style={styles.welcome}>
              <Text style={styles.title}>{i18n.t('welcome')}</Text>
              <Text style={[styles.title, { fontWeight: 'normal' }]}>
                {i18n.t('welcome_your_account')}
              </Text>
            </View>
            {problems.form ? (
              <View style={styles.errorBox}>
                <Text style={styles.errorText}>{i18n.t('login_invalid')}</Text>
              </View>
            ) : null}
            <View style={styles.form}>
              <TextInput
                style={BaseStyles.input}
                autoCapitalize="none"
                autoCompleteType="email"
                value={form.email}
                placeholder={i18n.t('email_address')}
                keyboardType="email-address"
                textContentType="emailAddress"
                onChangeText={(value) => {
                  setForm({ ...form, email: value })
                }}
              />
              <View style={styles.errorWrapper}>
                {problems.email ? (
                  <Text style={styles.errorText}>
                    {i18n.t('enter_valid_email')}
                  </Text>
                ) : null}
              </View>
              <TextInput
                style={[BaseStyles.input]}
                autoCapitalize="none"
                autoCompleteType="password"
                value={form.password}
                textContentType="password"
                placeholder={i18n.t('password')}
                secureTextEntry={true}
                returnKeyType="go"
                onSubmitEditing={makeLogin}
                onChangeText={(value) => {
                  setForm({ ...form, password: value })
                }}
              />
              <View style={styles.errorWrapper}>
                {problems.password ? (
                  <Text style={styles.errorText}>
                    {i18n.t('enter_password')}
                  </Text>
                ) : null}
              </View>
              <SubmitButton
                onPress={makeLogin}
                label={processing ? `${i18n.t('loading')}...` : i18n.t('login')}
              />
            </View>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
  },
  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  logo: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 140,
  },
  logoImage: {
    width: 100,
  },
  loginBox: {
    width: '100%',
    maxWidth: 400,
  },
  errorBox: {
    borderLeftColor: Colors.secondary,
    borderLeftWidth: 1,
    paddingHorizontal: 10,
  },
  errorText: {
    color: Colors.secondary,
  },
  errorWrapper: {
    marginBottom: 5,
  },
  title: {
    marginVertical: 15,
    color: Colors.primary,
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    marginRight: 5,
  },
  form: {
    backgroundColor: '#fff',
    width: '100%',
    maxWidth: 400,
    borderRadius: 4,
    overflow: 'hidden',
  },
  welcome: {
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'row',
    maxWidth: 400,
  },
})
