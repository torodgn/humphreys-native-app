import * as React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { useEffect, useState } from 'react'
import { user } from '../utility'
import i18n from '../config/i18n'

export default function Dashboard({ navigation }: any) {
  const [userData, setUserData] = useState({})

  const getUser = async () => {
    const result = await user()
    setUserData(result)
  }

  useEffect(() => {
    getUser()
  }, [])

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{i18n.t('dashboard')}</Text>
      <View style={styles.separator} />
      <Text>{JSON.stringify(userData, null, 2)}</Text>

      <TouchableOpacity
        onPress={async () => {
          navigation.navigate('Issuers')
        }}
        style={styles.button}
      >
        <Text style={styles.buttonText}>{i18n.t('issuers')}</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  button: {
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
})
