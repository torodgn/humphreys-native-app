import * as React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { ASYNC_STORAGE_KEYS } from '../constants/settings'
import { storage } from '../utility'
import i18n from '../config/i18n'
import BaseStyles from '../constants/BaseStyles'

export default function Account({ setSignedIn }: any) {
  const navigation = useNavigation()

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={[BaseStyles.button, styles.logout]}
        onPress={() => {
          storage.forget(ASYNC_STORAGE_KEYS.APP_ACCESS).then(() => {
            navigation.navigate('Main')
            setSignedIn(false)
          })
        }}
      >
        <Text style={BaseStyles.buttonText}>{i18n.t('logout')}</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  logout: {
    alignSelf: 'center',
  },
  separator: {
    marginVertical: 10,
    height: 1,
    width: '80%',
  },
})
