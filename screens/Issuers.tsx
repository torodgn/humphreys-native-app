import * as React from 'react'
import { useEffect, useState } from 'react'
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import i18n from '../config/i18n'
import Filters from './../components/Filters'
import { issuers } from '../utility'

const Item = ({ item, onPress }: any) => (
  <TouchableOpacity onPress={onPress} style={[styles.item]}>
    <Text style={styles.itemTitle}>{item.nombre}</Text>
  </TouchableOpacity>
)

export default function Issuers({ navigation }: any) {
  const [issuersData, setIssuersData] = useState([])
  const [pagination, setPagination] = useState({})
  const [page, setPage] = useState(1)
  const [loading, setLoading] = useState(false)

  const getIssuers = async () => {
    setLoading(true)

    issuers({
      page,
    }).then((result) => {
      let data = [...issuersData, ...result.data]

      setIssuersData(data)
      setPagination({
        last_page: result.last_page,
        total: result.total,
      })
      setLoading(false)
    })
  }

  const firstPage = async () => {
    setPage(1)
  }

  const nextPage = async () => {
    if (!pagination?.last_page || pagination.last_page >= page) {
      setPage((page) => page + 1)
    }
  }

  const prevPage = async () => {
    if (page > 1) {
      setPage((page) => page - 1)
    }
  }

  const loadMore = async () => {
    nextPage()
  }

  useEffect(() => {
    return navigation.addListener('focus', async () => {
      setPage(1)
      setIssuersData([])
      console.log('issuers focus')
      await firstPage()
    })
  }, [navigation])

  useEffect(() => {
    getIssuers()
  }, [page])

  const renderItem = ({ item }: any) => {
    return <Item item={item} onPress={() => alert(item.id)} />
  }

  const renderHeader = () => {
    return (
      <View>
        <View style={styles.titleWrapper}>
          <Text style={styles.title}>{i18n.t('issuers')}</Text>
          <Text style={styles.total}>{pagination.total}</Text>
        </View>
        <Filters current={{}}></Filters>
      </View>
    )
  }

  const renderFooter = () => {
    return loading ? (
      <View style={styles.loader}>
        <Text style={styles.title}>{i18n.t('loading')}</Text>
      </View>
    ) : (
      <></>
    )
  }

  return issuersData.length > 0 ? (
    <FlatList
      style={styles.container}
      data={issuersData}
      keyExtractor={(item) => `id-${item.id}`}
      renderItem={renderItem}
      ListHeaderComponent={renderHeader}
      ListFooterComponent={renderFooter}
      onEndReachedThreshold={0.4}
      onEndReached={loadMore}
    />
  ) : (
    <ScrollView style={styles.container}>
      <Text style={styles.title}>{i18n.t('loading')}...</Text>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f6fa',
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  total: {
    backgroundColor: '#fff',
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: '#eee',
    borderRadius: 20,
    overflow: 'hidden',
  },
  item: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: '#fff',
    borderRadius: 4,
    marginVertical: 4,
    borderWidth: 1,
    borderColor: '#eee',
    overflow: 'hidden',
  },
  itemTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  loader: {
    paddingVertical: 10,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  titleWrapper: {
    flex: 1,
    paddingVertical: 15,
    paddingHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})
