import { Colors, Icons } from '../constants/BaseStyles'
import { FontAwesome } from '@expo/vector-icons'
import i18n from '../config/i18n'
import Dashboard from './Dashboard'
import Services from './Services'
import Issuers from './Issuers'
import Files from './Files'
import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Header } from 'react-native-elements'
import { Image, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native'
const Tab = createBottomTabNavigator()

export default function Main() {
  const navigation = useNavigation()

  return (
    <>
      <Header
        backgroundColor="#fff"
        leftComponent={
          <Image
            resizeMode="contain"
            style={styles.logoImage}
            source={require('./../assets/images/logo.png')}
          />
        }
        rightComponent={
          <TouchableOpacity
            style={styles.account}
            onPress={() => {
              navigation.navigate('Account')
            }}
          >
            <FontAwesome name="user" size={16} color="#fff" />
          </TouchableOpacity>
        }
      />
      <SafeAreaView style={styles.innerContainer}>
        <Tab.Navigator
          initialRouteName="Issuers"
          screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
              let key = `${route.name}`
              let iconName = Icons[key]
              return <FontAwesome name={iconName} size={size} color={color} />
            },
          })}
          tabBarOptions={{
            style: {
              width: '100%',
              height: 64,
              flexDirection: 'column',
              alignSelf: 'center',
              //            paddingVertical: 10,
            },
            tabStyle: {
              height: 52,
              //marginBottom: 10,
            },
            activeTintColor: Colors.secondary,
            inactiveTintColor: 'gray',
          }}
        >
          <Tab.Screen
            name="Dashboard"
            options={{
              tabBarLabel: i18n.t('dashboard'),
            }}
            component={Dashboard}
          />
          <Tab.Screen
            name="Services"
            options={{
              tabBarLabel: i18n.t('services'),
            }}
            component={Services}
          />
          <Tab.Screen
            name="Issuers"
            options={{
              tabBarLabel: i18n.t('issuers'),
            }}
            component={Issuers}
          />
          <Tab.Screen
            name="Files"
            options={{
              tabBarLabel: i18n.t('files'),
            }}
            component={Files}
          />
        </Tab.Navigator>
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  logoImage: {
    width: 30,
  },
  account: {
    borderRadius: 50,
    backgroundColor: Colors.primary,
    paddingVertical: 7,
    paddingHorizontal: 10,
  },
  innerContainer: {
    flex: 1,
  },
})
