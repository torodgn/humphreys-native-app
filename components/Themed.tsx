import * as React from 'react';
import { Text as DefaultText, View as DefaultView } from 'react-native';

import BaseStyles from '../constants/BaseStyles';
import useColorScheme from '../hooks/useColorScheme';

