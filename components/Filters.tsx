import { Text, View, StyleSheet } from 'react-native'
import React, { useState } from 'react'
import RNPickerSelect from 'react-native-picker-select'
import { DefineDataParams } from '../types'
import i18n from '../config/i18n'

export default function Filters({ current }) {
  const [data, setdata] = useState(current)

  const defineData = ({ field, value }: DefineDataParams) => {
    data[field] = value
    setdata(data)
  }

  return (
    <View>
      <Text />
      <RNPickerSelect
        value={data?.estado || ''}
        onValueChange={(value) => defineData({ field: 'estado', value: value })}
        style={pickerSelectStyles}
        placeholder={{
          label: i18n.t('statuses'),
          value: null,
          color: '#9EA0A4',
        }}
        items={[
          { label: i18n.t('good'), value: 'bien' },
          { label: i18n.t('alert'), value: 'alerta' },
          { label: i18n.t('breaching'), value: 'incumpliendo' },
        ]}
      />
    </View>
  )
}


const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
