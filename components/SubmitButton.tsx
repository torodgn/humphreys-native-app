import { Text, TouchableOpacity } from 'react-native'
import BaseStyles from '../constants/BaseStyles'
import React from 'react'

export default function SubmitButton({ onPress, label }: any) {
  return (
    <TouchableOpacity style={BaseStyles.button} onPress={onPress}>
      <Text style={BaseStyles.buttonText}>{label}</Text>
    </TouchableOpacity>
  )
}
