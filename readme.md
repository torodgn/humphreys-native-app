## Resources 

[https://classic.yarnpkg.com/en/docs/install/](https://classic.yarnpkg.com/en/docs/install/)

[https://expo.io/](https://expo.io/)

[https://reactnative.dev/](https://reactnative.dev/)

## Install


```
yarn
```

## Run


```
yarn start
```

