export const ASYNC_STORAGE_KEYS = {
  APP_ACCESS: '@AppAccess',
}

export const CONFIG = {
  HOST: 'http://humphreys-app.lndo.site',
  //HOST: 'https://app.humphreysadvisors.cl',
  LOGIN_URI: '/oauth/token',
  USER_URI: '/api/user',
  ISSUERS_URI: '/api/emisores',
  CLIENT_SECRET: 'BQEY7DGhjRwTF734TCPH4j5cPJOTnNclhUL3KznJ',
  CLIENT_ID: 2,
  GRANT_TYPE: 'password',
}
