import { StyleSheet } from 'react-native'
import { IconOptions } from '../types'

const PrimaryColour = '#233162'
const SecondaryColour = '#c22626'

export const Colors = {
  primary: PrimaryColour,
  secondary: SecondaryColour,
}

export const Icons: IconOptions = {
  Dashboard: 'tachometer',
  Services: 'tasks',
  Issuers: 'paper-plane',
  Files: 'file-text',
}

export default StyleSheet.create({
  input: {
    borderBottomWidth: 1,
    borderColor: 'grey',
    paddingVertical: 10,
    marginTop: 10,
    marginBottom: 3,
    fontSize: 18,
  },
  button: {
    backgroundColor: Colors.secondary,
    paddingVertical: 10,
    paddingHorizontal: 40,
    borderRadius: 50,
    alignItems: 'center',
    alignSelf: 'flex-end',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
})
